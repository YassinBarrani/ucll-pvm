\usepackage{../pvm}
\usetikzlibrary{shadows,shapes.multipart}

\title{Classes: Constructors}
\author{Fr\'ed\'eric Vogels}

\newcommand{\highlightbox}[2][]{
  \draw[opacity=.75,ultra thick,red,#1] ($ (#2.south west) + (-.1,-.1) $) rectangle ($ (#2.north east) + (.1,.1) $)
}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Special Constructors}
  \begin{itemize}
    \item \cpp\ has a few ``special'' constructors
    \item They get called in specific circumstances
    \item New stuff in \cpp11!
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Default Constructor}
  \begin{itemize}
    \item Constructor with zero parameters
  \end{itemize}
  \code[frame=lines,width=.6\linewidth]{default-constructor-example.cpp}
\end{frame}

\begin{frame}
  \frametitle{Default Constructor: When Is It Called?}
  \begin{itemize}
    \item When creating an object without giving arguments
    \item Array creation
  \end{itemize}
  \vskip5mm
  \structure{Examples}
  \code[frame=lines,width=.9\linewidth]{default-constructor-calls.cpp}
\end{frame}

\begin{frame}
  \frametitle{Default Constructor: Automatic Generation}
  \begin{itemize}
    \item Generated automatically if you do not define any constructors yourself
          \begin{itemize}
            \item Calls base constructors
            \item Calls default constructors on member variables
            \item Does \emph{not} initialise member variables to zero! \\ Default constructor for {\tt int} does not set value to {\tt 0}
          \end{itemize}
    \item Generation \link{http://en.cppreference.com/w/cpp/language/default_constructor}{rules} are quite complex
    \item It is easier to be explicit about default constructors
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Default Constructor}
  \begin{itemize}
    \item Forcing no default constructor
          \vskip1mm
          \code[frame=lines,font size=\small]{no-default-constructor.cpp}
    \item Force default constructor generation
          \vskip1mm
          \code[frame=lines,font size=\small]{force-default-constructor.cpp}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Default Constructor}
  \begin{itemize}
    \item Custom default constructor
          \vskip1mm
          \code[frame=lines,font size=\small]{custom-default-constructor.cpp}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Default Constructor: Quiz}
  \code[frame=lines]{default-constructor.cpp}
  \visible<2->{
    \begin{center}
      Does not compile! \\ Declaration of {\tt foo} misinterpreted as function declaration \\
      Known as \link{https://en.wikipedia.org/wiki/Most_vexing_parse}{Most Vexing Parse}
    \end{center}
  }
\end{frame}

\begin{frame}
  \frametitle{Default Constructor: Quiz}
  \code[frame=lines]{default-constructor2.cpp}
\end{frame}

\begin{frame}
  \frametitle{Default Constructor: Quiz}
  \code[frame=lines]{default-constructor3.cpp}
  \visible<2->{
    \begin{center}
      Does not compile! Parentheses are necessary here!
    \end{center}
  }
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor}
  \begin{itemize}
    \item Constructor taking {\tt const T\&}
    \item Should make (deep) copy of given object
    \item Called whenever a copy needs to be made
  \end{itemize}
  \vskip5mm
  \code[frame=lines]{copy-constructor.cpp}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: Automatic Generation}
  \begin{itemize}
    \item Automatically generated, similar \link{http://en.cppreference.com/w/cpp/language/copy_constructor}{rules} to default constructor
    \item Can be explicitly generated (using {\tt = default})
    \item Can be explicitly omitted (using {\tt = delete})
  \end{itemize}
  \code[frame=lines,font size=\small]{implicit-copy-constructor.cpp}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: When Is It Called?}
  \begin{itemize}
    \item When copy of object is needed
    \item In some cases, \cpp\ standard allows \link{https://en.wikipedia.org/wiki/Return_value_optimization}{optimisations}
          that change the behaviour of the program, so maybe copy constructor does not even get called
    \item It is therefore important not to do exotic things in the copy constructor besides copying the object
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: When Is It Called?}
  \code[frame=lines]{copy-constructor1.cpp}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: When Is It Called?}
  \code[frame=lines]{copy-constructor2.cpp}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: When Is It Called?}
  \code[frame=lines]{copy-constructor3.cpp}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: When Is It Called?}
  \code[frame=lines]{copy-constructor4.cpp}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: When Is It Called?}
  \code[frame=lines]{copy-constructor5.cpp}
\end{frame}

\begin{frame}
  \frametitle{Copy Constructor: When Is It Called?}
  \code[frame=lines]{copy-constructor6.cpp}
\end{frame}

\begin{frame}
  \frametitle{Move Constructor}
  \begin{itemize}
    \item Introduced in \cpp11
    \item Optimisation
    \item Called when object is ``moved''
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example}
  \code[frame=lines]{move-example.cpp}
  \begin{itemize}
    \item When {\tt foo} returns, {\tt ns} must be copied to {\tt result}
    \item Since {\tt foo} returns, {\tt ns} must be destroyed too
    \item So, we're making a copy only to destroy the original
    \item Why copy at all?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Move Constructor}
  \begin{itemize}
    \item Move constructor can be used in such circumstances
    \item One object ``takes over'' all data from the original
    \item The original object is ``emptied''
    \item Emptying original is important, as its destructor will still be called
  \end{itemize}
  \code[frame=lines,width=.6\linewidth]{move-constructor-syntax.cpp}
\end{frame}

\begin{frame}
  \frametitle{Example}
  \code[frame=lines,font size=\small]{move-constructor.cpp}
\end{frame}

\begin{frame}
  \frametitle{Visualisation: Copy Constructor}
  \begin{center}
    \begin{tikzpicture}[object/.style={draw,minimum width=3cm,minimum height=.75cm}]
      \visible<1-20>{
        \node[object] (v1) at (0,0) {\tt vector<int>};
      }

      \visible<1-19>{
        \draw[xstep=.25cm,ystep=.25cm,xshift=-0.125cm] (0,-1) grid (0.25,-5);
        \draw[-latex] (v1.south) -- (0,-1);
        \foreach[count=\i] \c in {a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p} {
          \tikzmath{
            real \j;
            \j = -1 - 0.25 * \i + 0.125;
          }

          \node[font=\tiny] at (0,\j) {\c};
        }
      }

      \visible<2-21>{
        \node[object] (v2) at (5,0) {\tt vector<int>};
      }

      \visible<3->{
        \draw[xstep=.25cm,ystep=.25cm,xshift=4.875cm] (0,-1) grid (0.25,-5);
        \draw[-latex] (v2.south) -- (5,-1);
      }

      \foreach[count=\i] \c in {a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p} {
        \tikzmath{
          int \slideindex;
          real \j;
          \slideindex = int(\i + 3);
          \j = -1 - 0.25 * \i + 0.125;
        }

        \visible<\slideindex>{
          \draw[-latex] (0,\j) -- (5,\j);
        }

        \visible<\slideindex->{
          \node[font=\tiny] at (5,\j) {\c};
        }
      }
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Visualisation: Move Constructor}
  \begin{center}
    \begin{tikzpicture}[object/.style={draw,minimum width=3cm,minimum height=.75cm}]
      \visible<1-3>{
        \node[object] (v1) at (0,0) {\tt vector<int>};
      }

      \draw[xstep=.25cm,ystep=.25cm,xshift=-.125cm] (0,-1) grid (0.25,-5);
      \foreach[count=\i] \c in {a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p} {
        \tikzmath{
          real \j;
          \j = -1 - 0.25 * \i + 0.125;
        }

        \node[font=\tiny] at (0,\j) {\c};
      }

      \visible<1-2>{
        \draw[-latex] (v1.south) -- (0,-1);
      }

      \visible<2-4>{
        \node[object] (v2) at (5,0) {\tt vector<int>};
      }

      \visible<3>{
        \draw[-latex] (v2.south) -- (0,-1) node[midway,below,sloped] {yoink!};
      }

      \visible<4>{
        \draw[-latex] (v2.south) -- (0,-1);
      }
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Unary Constructors}
  \begin{itemize}
    \item Unary constructors: constructors with single argument
    \item Act as implicit casts
  \end{itemize}
  \vskip5mm
  \begin{overprint}
    \onslide<handout:1|1>
    \code[frame=lines,font size=\small]{implicit1.cpp}
    \onslide<handout:2|2>
    \code[frame=lines,font size=\small]{implicit2.cpp}
  \end{overprint}
\end{frame}

\begin{frame}
  \frametitle{Preventing Implicit Casts}
  \begin{itemize}
    \item Generally, you don't want implicit casts
    \item Add {\tt explicit} before unary constructor
  \end{itemize}
  \code[frame=lines,font size=\small]{explicit.cpp}
\end{frame}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "constructors"
%%% End:
