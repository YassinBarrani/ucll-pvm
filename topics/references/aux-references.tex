\usepackage{../pvm}
\usetikzlibrary{shadows,shapes.multipart}

\title{References}
\author{Fr\'ed\'eric Vogels}

\newcommand{\highlightbox}[2][]{
  \draw[opacity=.75,ultra thick,red,#1] ($ (#2.south west) + (-.1,-.1) $) rectangle ($ (#2.north east) + (.1,.1) $)
}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Pointers}
  \begin{procontralist}
    \pro Efficient way to give access to large amounts of data
    \pro Allow to give write-access to own variables
    \con Clumsy syntax
  \end{procontralist}
  \vskip5mm
  \structure{Example (peek into future)}
  \code[width=.95\linewidth,font size=\footnotesize,frame=lines]{operator-overloading-bad.cpp}
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
    \item Not in C, new in \cpp
    \item Gives same ``power'' as pointers
    \item Uses pointers behind the scenes
    \item Much cleaner syntax
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Syntax}
  \begin{itemize}
    \item {\tt T}: some type
    \item {\tt T[]}: array of {\tt T}
    \item {\tt const T}: readonly view of {\tt T}
    \item {\tt T*}: pointer to {\tt T}
    \item<2-> {\tt T\& }: reference to {\tt T}
  \end{itemize}
  \visible<3->{
    \begin{framed}
      \begin{center} \Large
        \textbf{Warning} \\[2mm]
        Do not confuse with address-of operator!
      \end{center}
    \end{framed}
  }
\end{frame}

\begin{frame}
  \frametitle{Semantics}
  \code[frame=lines,width=.5\linewidth]{by-value.cpp}
  \begin{itemize}
    \item {\tt foo} receives \emph{copy} of {\tt x}
    \item {\tt foo} only has access to the {\tt value} of the variable
    \item {\tt foo} cannot modify {\tt x} in any way
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Semantics}
  \code[frame=lines,width=.5\linewidth]{by-pointer.cpp}
  \begin{itemize}
    \item {\tt foo} receives the \emph{address} of {\tt x}
    \item {\tt foo} has access to the {\tt value} of {\tt x} (e.g.\ {\tt int y = *arg})
    \item {\tt foo} can modify {\tt x} (e.g.\ {\tt *arg = 6})
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Semantics}
  \code[frame=lines,width=.5\linewidth]{by-reference.cpp}
  \begin{itemize}
    \item {\tt foo} receives a \emph{reference} to {\tt x}
    \item In essence, this is the same as receiving the address of {\tt x}
    \item {\tt foo} has access to the {\tt value} of {\tt x} (e.g.\ {\tt int y = arg})
    \item {\tt foo} can modify {\tt x} (e.g.\ {\tt arg = 6})
    \item Whenever {\tt foo} interacts with {\tt arg}, it is interacting with {\tt x}
    \item References offer pointer-functionality with cleaner syntax
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Quiz}
  \code[font size=\small,frame=lines,width=.9\linewidth]{alias.cpp}
\end{frame}

\begin{frame}
  \frametitle{Quiz}
  \code[font size=\small,frame=lines,width=.9\linewidth]{byref.cpp}
\end{frame}

\begin{frame}
  \frametitle{When To Use References}
  \begin{itemize}
    \item Power of pointers
    \item Without the ugliness
    \item Why not always use references instead of pointers?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Being Explicit About Intentions}
  \code[frame=lines,width=.5\linewidth]{explicit.cpp}
  \begin{itemize}
    \item What guarantees do we have?
    \item If {\tt foo} takes {\tt int}, we know {\tt x} will not change
    \item If {\tt foo} takes {\tt int\&}, {\tt x} will probably change
    \item We cannot infer which is true based on syntax of {\tt foo(x)}
    \item Working with pointers is more explicit
          \begin{itemize}
            \item {\tt foo(x)}: {\tt x} will not change
            \item {\tt foo(\&x)}: {\tt x} will probably change
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Rules of Thumb}
  \begin{itemize}
    \item Readonly access to small data: use {\tt T}
          \begin{itemize}
            \item Value will be copied
            \item Gives readonly access to variable
            \item Only efficient if {\tt T}s are small
          \end{itemize}
    \item Readonly access to large data: use {\tt const T\&}
          \begin{itemize}
            \item {T\&} is as efficient as passing a pointer
            \item {T\&} gives ``full access''
            \item {const T\&} restricts access to readonly
            \item Efficient and safe
          \end{itemize}
    \item Read/write access: use {\tt T*}
          \begin{itemize}
            \item Efficient
            \item Gives full access (read + write)
          \end{itemize}
  \end{itemize}  
\end{frame}

\begin{frame}
  \frametitle{Quiz}
  \code[frame=lines]{return-reference.cpp}
\end{frame}

\begin{frame}
  \frametitle{Returning References}
  \code[frame=lines,width=.5\linewidth]{return-value.cpp}
  \begin{itemize}
    \item Does not compile
    \item Nonsensical
    \item {\tt foo} returns value {\tt 5}
    \item We are trying to assign {\tt 6} to {\tt 5}
    \item What could this even mean?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Returning References}
  \code[frame=lines,width=.5\linewidth]{return-reference2.cpp}
  \begin{itemize}
    \item Compiles just perfectly
    \item {\tt foo} does return a reference to {\tt x}
    \item We do not just get the value of {\tt x}
    \item We also get its location in memory
    \item We receive a complete variable, not just a value
    \item Assigning to a variable makes perfect sense
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Use Cases}
  \code[language=java,frame=lines,font size=\small]{use-case-arraylist.java}
\end{frame}

\begin{frame}
  \frametitle{Use Cases (Advanced Example)}
  \code[frame=lines]{use-case-vectors.cpp}
\end{frame}

\begin{frame}
  \frametitle{Summary}
  \begin{center}
    \Large Reference to {\tt T}, written {\tt T\&}
  \end{center}
  \begin{itemize}
    \item Usage syntax same as ordinary variable
    \item Works with pointer behind-the-scenes
    \item Passes along the entire variable instead of just its value
  \end{itemize}
  \vskip5mm
  \begin{center}
    \begin{tabular}{lccc}
      & \textbf{By value} & \textbf{By pointer} & \textbf{By reference} \\
      \toprule
      \textbf{Type} & {\tt T x = init} & {\tt T* x = \&init} & {\tt T\& x = init} \\
      \textbf{Reading} & {y = x} & {\tt y = *x} & {\tt y = x} \\
      \textbf{Writing} & {x = y} & {\tt *x = y} & {\tt x = y} \\
    \end{tabular}
  \end{center}
\end{frame}


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "references"
%%% End:
